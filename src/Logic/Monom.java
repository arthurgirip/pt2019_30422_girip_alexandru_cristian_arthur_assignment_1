package Logic;

public class Monom {
    private double coefficient;
    private double power;

    public Monom(double coef, double power)
    {
        this.coefficient = coef;
        this.power = power;
    }

    public double getPower()
    {
        return this.power;
    }

    public double getCoeff()
    {
        return this.coefficient;
    }

    public Monom div (Monom aux)
    {
        Monom imp = new Monom(this.coefficient / aux.getCoeff(), power - aux.getPower());
        return imp;
    }


}