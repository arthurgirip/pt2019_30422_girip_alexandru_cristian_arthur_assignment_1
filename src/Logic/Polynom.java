package Logic;

import java.util.ArrayList;
import java.util.List;

public class Polynom {

    private int nrMon= 0;
    public List<Monom> arg = new ArrayList<Monom>();

    public void addMonom(Monom x)
    {
        arg.add(x);
        nrMon++;
    }


    public int nrMonoms()
    {
        return nrMon;
    }

    public Monom getMonom(int i)
    {
        return arg.get(i);
    }

    public void elimMonom(int i)
    {
        arg.remove(i);
    }

    public Monom getMonWithP(double n)
    {
        for(Monom m : arg)
        {
            if(m.getPower() == n)
                return m;
        }
        return null;
    }

    public boolean isempty()
    {
        if(arg.isEmpty()) return true;
        else return false;
    }

    public void reset()
    {
        arg.clear();
        nrMon = 0;
    }

    public String toString()
    {
        StringBuilder rez = new StringBuilder();
        for(Monom m : this.arg)
        {
            if(m.getPower()!=0 && m.getCoeff()!=0) {
                if (m.getCoeff() < 0)
                    rez.append("-");
                else if (m.getCoeff() > 0 && arg.indexOf(m) != 0)
                    rez.append("+");

                rez.append(String.valueOf(m.getCoeff()));
                if(m.getPower() == 1)
                    rez.append(" x");
                else {
                    rez.append(" x^");
                    rez.append(String.valueOf(m.getPower()));
                }

            }
            else if(m.getPower() ==0 && m.getCoeff()!= 0)
            {
                rez.append(" " + String.valueOf(m.getCoeff()));
            }
            else if(m.getCoeff() == 0)
            {
                arg.remove(m);
            }
        }
        return rez.toString();
    }




}
