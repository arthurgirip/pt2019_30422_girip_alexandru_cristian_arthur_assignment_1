package Logic;

public class Operations {

    public static Polynom addition(Polynom p1, Polynom p2)
    {
        Polynom rez = new Polynom();

        for(Monom mon1 : p1.arg)
        {
            for(Monom mon2 : p2.arg)
            {
                if(mon1.getPower() == mon2.getPower())
                {
                    Monom rm = new Monom(mon1.getCoeff()+mon2.getCoeff(),mon1.getPower());
                    rez.addMonom(rm);
                }
            }
        }

        return rez;
    }

    public static Polynom subtract(Polynom p1, Polynom p2)
    {
        Polynom rez = new Polynom();
        Polynom added = new Polynom();

        for(Monom m: p1.arg)
        {
            Monom mon = p2.getMonWithP(m.getPower());
            if(mon!=null)
            {
                added.addMonom(mon);
                rez.addMonom(new Monom(m.getCoeff()-mon.getCoeff(),m.getPower()));
            }
            else
                rez.addMonom(m);
        }
        for(Monom m : p2.arg)
        {
            if(!added.arg.contains(m))
                rez.addMonom(new Monom(m.getCoeff()*-1,m.getPower()));
        }
        return rez;
    }


    public static Polynom multiply(Polynom p1, Polynom p2)
    {
        Polynom rez = new Polynom();

        for(Monom mon1 : p1.arg)
        {
            for(Monom mon2 : p2.arg)
            {
                Monom mul = new Monom(mon1.getCoeff()*mon2.getCoeff(),mon1.getPower()+mon2.getPower());

                rez.addMonom(mul);
            }
        }

        return rez;
    }

    public static Polynom integrate(Polynom p1)
    {
        Polynom rez = new Polynom();

        for(Monom m : p1.arg)
            rez.addMonom(new Monom(m.getCoeff()/(m.getPower() + 1),m.getPower()+1));

        return rez;
    }

    public static Polynom derivate(Polynom p1)
    {
        Polynom rez = new Polynom();
        int i=0;
        try{
            int nr = p1.nrMonoms();
            while(i < nr) {
                double coefP = p1.getMonom(i).getCoeff();
                double powerP = p1.getMonom(i).getPower();
                i++;

                if(powerP != 0){
                    Monom m = new Monom(coefP * powerP, powerP-1);
                    rez.addMonom(m);}
            }
        }
        catch(Exception E) {}

        return rez;
    }





}
