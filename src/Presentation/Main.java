package Presentation;

import Logic.Monom;
import Logic.Operations;
import Logic.Polynom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {



    Polynom[] pol = new Polynom[10];
    static JFrame f = new JFrame("Polynomial Calculator");


    static JLabel label_POLYNOM_1 = new JLabel("Polynom 1 : ");
    static JLabel label_POLYNOM_2 = new JLabel("Polynom 2 : ");
    static JLabel label_POLYNOM_UNARY_REZ = new JLabel("Result for unary: ");
    static JLabel label_POLYNOM_BINARY_REZ = new JLabel("Result for binary: ");

    static JTextField text_POLYNOM_1 = new JTextField(15);
    static JTextField text_POLYNOM_2 = new JTextField(15);
    static JTextField text_POLYNOM_UNARY_REZ = new JTextField(20);
    static JTextField text_POLYNOM_BINARY_REZ = new JTextField(20);

    static JButton button_ADD = new JButton("Add");
    static JButton button_SUBTRACTION = new JButton("Subtract");
    static JButton button_MULTIPLICATION = new JButton("Multiply");
    static JButton button_DIVISION = new JButton("Divide");
    static JButton button_DERIVATE = new JButton("Differentiate");
    static JButton button_INTEGRATE = new JButton("Integrate");

    static GridBagLayout grid = new GridBagLayout();
    static GridBagConstraints c = new GridBagConstraints();

    public static void DOWN()
    {
        c.gridy++;
    }

    public static void RIGHT()
    {
        c.gridx++;
    }

    public static void LEFT()
    {
        c.gridx--;
    }

    public static void UP()
    {
        c.gridy--;
    }

    public static void RESET()
    {
        c.gridx = 0;
        c.gridy = 0;
    }


    public static void createGUI()
    {
        f.setSize(600,600);
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setLayout(grid);

        c.gridy = 0;
        c.gridx = 0;

        f.add(label_POLYNOM_1,c);

        RIGHT();

        f.add(text_POLYNOM_1,c);

        RIGHT();

        f.add(label_POLYNOM_2,c);

        RIGHT();

        f.add(text_POLYNOM_2,c);

        RESET();
        DOWN();
        RIGHT();

        f.add(label_POLYNOM_UNARY_REZ,c);

        DOWN();

        f.add(label_POLYNOM_BINARY_REZ,c);

        UP();
        RIGHT();

        f.add(text_POLYNOM_UNARY_REZ,c);

        DOWN();

        f.add(text_POLYNOM_BINARY_REZ,c);

        DOWN();
        DOWN();
        LEFT();
        LEFT();
        button_ADD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    text_POLYNOM_BINARY_REZ.setText(Operations.addition(toPoly(text_POLYNOM_1.getText()),toPoly(text_POLYNOM_2.getText())).toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        f.add(button_ADD,c);

        RIGHT();
        button_SUBTRACTION.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    text_POLYNOM_BINARY_REZ.setText(Operations.subtract(toPoly(text_POLYNOM_1.getText()),toPoly(text_POLYNOM_2.getText())).toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        f.add(button_SUBTRACTION,c);

        RIGHT();
        button_MULTIPLICATION.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    text_POLYNOM_BINARY_REZ.setText(Operations.multiply(toPoly(text_POLYNOM_1.getText()),toPoly(text_POLYNOM_2.getText())).toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        f.add(button_MULTIPLICATION,c);

        RIGHT();
        f.add(button_DIVISION,c);

        DOWN();
        LEFT();
        LEFT();
        LEFT();
        button_DERIVATE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    text_POLYNOM_UNARY_REZ.setText(Operations.derivate(toPoly(text_POLYNOM_1.getText())).toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });

        f.add(button_DERIVATE,c);
        RIGHT();
        RIGHT();
        RIGHT();
        button_INTEGRATE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    text_POLYNOM_UNARY_REZ.setText(Operations.integrate(toPoly(text_POLYNOM_1.getText())).toString());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        f.add(button_INTEGRATE,c);



        f.pack();
        f.setVisible(true);
    }

    public static Polynom toPoly(String s) throws Exception
    {
        Polynom pol = new Polynom();

        s=s.replaceAll("\\s","");
        while(s.contains("++") || s.contains("+=") || s.contains("-+") || s.contains("--"))
        {
            s=s.replaceAll("\\+-", "-");
            s=s.replaceAll("-\\+", "-");
        }

        s=s.replaceAll("-", "+-");
        String[] parts = s.split("\\+");

        for(String i : parts)
        {
            if(!i.isEmpty())
            {
                if(i.matches("^(-)?(\\d+)?(\\*)?(x|X)\\^\\d+$"))
                {
                    String[] otherParts = i.split("(\\*)?(x|X)\\^");

                    if(otherParts[0].equals(""))
                        otherParts[0]="1";

                    if(otherParts[0].equals("-"))
                        otherParts[0]="-1";

                    pol.addMonom(new Monom(Integer.valueOf(otherParts[0]),Integer.valueOf(otherParts[1])));

                }

                else
                    {
                        if(i.matches("^(-)?\\d+(\\*)?(x|X)$"))
                        {
                            String[] otherParts = i.split("(\\*)?(x|X)");

                            pol.addMonom(new Monom(Integer.valueOf(otherParts[0]),1));
                        }
                        else if(i.matches("^(-)?(x|X)$"))
                        {

                            if(i.matches("(x|X)"))
                                pol.addMonom(new Monom(1,1));

                            if(i.matches("-(x|X)"))
                                pol.addMonom(new Monom(-1,1));

                        }
                        else
                            {
                                throw new Exception("WRONG FORMAT");
                            }
                    }
            }
        }

        return pol;
    }




    public static void main(String[] argc)
    {
        createGUI();

    }


}
